# Resume

The `master` branch should always point to the latest up to date copy of the Resume PDF.  The PDF in this repo is my canonical resume, and is derived from either a ODT file or a Google Doc.

## ODT

The ODT file may be derived from the Google Docs version of my resume.  Currently 
I'm having an issue getting Google Docs to export some italicized fonts correctly (something in the positioning gets messed up), 
so the canonical PDF is generated directly from the ODT file that I download and edit locally.
